from django.contrib import admin

# Register your models here.
from .models import CryptoFile

class CryptoFileModelAdmin(admin.ModelAdmin):
	list_display = ["id","filename", "slug", "file", "description", "timestamp"]
	list_display_links = ["filename"]
	list_editable = ["description"]
	# list_filter = ["updated", "timestamp"]

	search_fields = ["filename"]
	class Meta:
		model = CryptoFile


admin.site.register(CryptoFile, CryptoFileModelAdmin)