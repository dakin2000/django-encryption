from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.core.files import File


from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
    )
   
from djangoencryptfile.django_encrypt_file import EncryptionService, ValidationError


# Create your views here.
from .forms import FileForm,DecryptFileForm

from .models import CryptoFile
from .utils import encrypt_file,decrypt_file

@login_required
def doc_list_view(request):
	queryset = CryptoFile.objects.all()
	context = {
		'cryptofiles':queryset,
	}
	return render(request,'index.html', context)

@login_required
def file_create(request):
	# if not request.user.is_staff or not request.user.is_superuser:
	# 	raise Http404
		
	form = FileForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()

		# my_object = CryptoFile.objects.filter(pk=instance.id).first()
		# password = my_object.password
		# service = EncryptionService(raise_exception=False)
		# with open(my_object.file.path, 'rb') as inputfile:
		# 	usefile = File(inputfile, name=my_object.file.path)
		# 	encrypted_file = service.encrypt_file(usefile, password, extension='enc')
		# 	decrypt_file = service.decrypt_file(encrypted_file, password, extension='enc')
		# 	print "this is an {0} of type {1}".format(encrypted_file, type(encrypted_file))
		# 	my_object.encrypted_file = encrypted_file

		# my_object.save()
		# print my_object.encrypted_file
			# message success
		messages.success(request, "Successfully Created")
		return HttpResponseRedirect(instance.get_absolute_url())
		
		
	context = {
		"form": form,
	}
	return render(request, "encrypt_new.html", context)

@login_required
def file_detail(request, slug=None):
	instance = get_object_or_404(CryptoFile, slug=slug)
	# with open(instance.encrypted_file.path) as f:
	# 	output = f.readlines()

	output = None

	form = DecryptFileForm()
	if request.method == 'POST':
		form = DecryptFileForm(request.POST)
		if form.is_valid():
			password = form.cleaned_data['password']
			print instance.password, password
			if instance.password == password:
				messages.success(request, "ACCESS GRANTED", extra_tags='html_safe')
				with open(instance.file.path) as f:
					output = f.readlines()
					# print output
				# return HttpResponseRedirect(instance.get_absolute_url())
	
	context = {
		"output":output,
		"title": instance.filename,
		"instance": instance,
		"form":form,
	}
	return render(request, "file_detail.html", context)

@login_required
def file_delete(request, slug=None):
	instance = get_object_or_404(CryptoFile, slug=slug)
	# if request.user != instance.user:
	# 	raise Http404
	instance.delete()
	messages.success(request, "Successfully deleted")
	return redirect("list")




@login_required
def decrypt_file(request, slug=None):
	instance = get_object_or_404(CryptoFile, slug=slug)
	form = DecryptFileForm(request.POST or None,instance=instance)
	if form.is_valid():
		print 'HELLO'
		instance = form.save(commit=False)

		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		"title": instance.filename,
		"instance": instance,
	}
	return render(request, "file_detail.html", context)


@login_required
def encrypt_file(request, slug=None):
	instance = get_object_or_404(CryptoFile, slug=slug)
	form = DecryptFileForm(request.POST or None,instance=instance)
	if form.is_valid():
		print 'HELLO'
		instance = form.save(commit=False)

		instance.save()
		messages.success(request, "<a href='#'>Item</a> Saved", extra_tags='html_safe')
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		"title": instance.filename,
		"instance": instance,
	}
	return render(request, "file_detail.html", context)