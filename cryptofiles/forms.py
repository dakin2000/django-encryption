from django import forms


from .models import CryptoFile


class FileForm(forms.ModelForm):
    filename = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'inputEmail',\
                                     'type': 'text',\
                                      'placeholder': 'Enter Filename'}),
                                max_length=30,
                                required=True,
                                help_text='Usernames may contain <strong>alphanumeric</strong>,\
                                             <strong>_</strong> and <strong>.</strong> characters')
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'id': 'inputPassword',\
                                     'type': 'text',\
                                      'placeholder': 'Short discription of the file...'}),
                                max_length=30,
                                required=True,
                                help_text='Usernames may contain <strong>alphanumeric</strong>,\
                                             <strong>_</strong> and <strong>.</strong> characters')
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'id':'Password', 'type':'password', 'placeholder': 'Password'}))
    
    # filename = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'inputFilename',\
    #                                  'type': 'text',\
    #                                   'placeholder': 'Enter Filename'}),
    #                             required=True,
    #                             help_text='Please enter the Description of the file you want to upload')
    # description = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'inputDescription',\
    #                                  'type': 'text',\
    #                                   'placeholder': 'Description'}),
    #                             required=True,
    #                             help_text='Please enter the Description of the file you want to upload')
    # publish = forms.DateField(widget=forms.SelectDateWidget)
    class Meta:
        model = CryptoFile
        fields = [
            "filename",
            "description",
            "password",
            "file",
        ]


class DecryptFileForm(forms.Form):
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 
                                    'id':'Password', 'type':'password', 
                                    'placeholder': 'Password'}),
        label="Password",
        max_length=100)
    