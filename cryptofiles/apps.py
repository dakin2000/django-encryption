from django.apps import AppConfig


class CryptofilesConfig(AppConfig):
    name = 'cryptofiles'
