from django.db import models
from django.conf import settings

from django.core.urlresolvers import reverse

from django.utils.text import slugify

from django.db.models.signals import pre_save



# Create your models here.



def upload_location(instance, filename):
    Model = instance.__class__
    i = CryptoFile.objects.order_by("id").last()
    return "%s/original/%s" %(i, filename)

def enc_upload_location(instance, filename):
    Model = instance.__class__
    i = CryptoFile.objects.order_by("id").last()
    return "%s/encrypted/%s" %(i, filename)

class CryptoFile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    filename = models.CharField(max_length=120,blank=False, null=False)
    slug = models.SlugField(unique=True)
    password = models.CharField(max_length=120,blank=False, null=False)
    # key = models.CharField(max_length=120,blank=False, null=False)
    file = models.FileField(upload_to=upload_location,null=True, blank=True)
    encrypted_file = models.FileField(upload_to=enc_upload_location,null=True, blank=True)
    description =  models.CharField(max_length=250,blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add = True, auto_now=False)

    def __unicode__(self):
        return self.filename


    def __str__(self):
        return self.filename


    class Meta:
        ordering = ["-timestamp"]


    def get_absolute_url(self):
        return reverse("detail", kwargs={"slug": self.slug})



def create_slug(instance, new_slug=None):
    slug = slugify(instance.filename)
    if new_slug is not None:
        slug = new_slug
    qs = CryptoFile.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

pre_save.connect(pre_save_post_receiver, sender=CryptoFile)
